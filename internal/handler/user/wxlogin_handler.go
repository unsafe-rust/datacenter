package user

import (
	"net/http"

	"gitee.com/gopher2011/datacenter/internal/logic/user"
	"gitee.com/gopher2011/datacenter/internal/svc"
	"gitee.com/gopher2011/datacenter/internal/types"
	"github.com/tal-tech/go-zero/rest/httpx"
)

func WxloginHandler(ctx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.WxLoginReq
		if err := httpx.Parse(r, &req); err != nil {
			httpx.Error(w, err)
			return
		}

		l := user.NewWxloginLogic(r.Context(), ctx)
		resp, err := l.Wxlogin(req)
		if err != nil {
			httpx.Error(w, err)
		} else {
			httpx.OkJson(w, resp)
		}
	}
}
