package config

import (
	"github.com/tal-tech/go-zero/core/stores/cache"
	"github.com/tal-tech/go-zero/rest"
)

type Config struct {
	rest.RestConf
	Database struct {
		Pkg             string
		DriverName      string
		DSN             string
		MaxOpenConn     int
		MaxIdleConn     int
		ConnMaxLifetime int
		ConnMaxIdleTime int
	}
	Logger struct {
		Path     string
		MaxAge   int
		Interval int
	}
	CacheRedis cache.ClusterConf
	Auth       struct {
		AccessSecret string
		AccessExpire int64
	}
}
