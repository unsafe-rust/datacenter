# 1、生成 `datacenter.api` 文件 (在终端执行命令)
````shell
mkdir datacenter && goctl api -o datacenter.api
````

# 2、编写 `datacenter.api` 文件
````shell
syntax = "v1"

info(
	title: "中台系统"// TODO: add title
	desc: "中台系统"// TODO: add description
	author: "li ou"
	email: "3212728662@qq.com"
)

// 获取应用信息
type Beid {
	Beid int64 `json:"beid"`
}
// 返回值
type Token {
	Token string `json:"token"`
}
// 微信活动
type WxTicket {
	Ticket string `json:"ticket"`
}

// 应用信息
type Application {
	Sname       string `json:"Sname"`       //名称
	Logo        string `json:"logo"`        //login
	IsClose     int64  `json:"isclose"`     //是否关闭
	Fullwebsite string `json:"fullwebsite"` //全站名称
}

//获取分享的请求参数
type SnsReq {
	Beid
	Ptyid   int64  `json:"ptyid"`    //对应平台
	BackUrl string `json:"back_url"` //登陆返回的地址
}

type SnsResp {
	Beid
	Ptyid    int64  `json:"ptyid"`     //对应平台
	Appid    string `json:"appid"`     //sns 平台的id
	Title    string `json:"title"`     //名称
	LoginUrl string `json:"login_url"` //微信登陆的地址
}

//获取分享的返回数据
type WxShareResp {
	Appid     string `json:"appid"`
	Timestamp int64  `json:"timestamp"`
	Noncestr  string `json:"noncestr"`
	Signature string `json:"signature"`
}

// 这是 datacenter 这个服务最外部的http服务
service datacenter-api {
	@doc(
		summary: "七牛云上传凭证"
	)
	@handler qiuniuToken
	post /common/qiuniu/token (Beid) returns (Token)
}
````
# 3、根据 `datacenter.api` 生成代码 (在终端执行如下命令)
````shell
goctl api go -api datacenter.api -dir .
````
* 生成的目录结构如下 (tree)
````shell
.
├── READEME.md
├── datacenter.api
├── datacenter.go
├── etc
│   └── datacenter-api.yaml
├── go.mod
├── go.sum
└── internal
    ├── config
    │   └── config.go
    ├── handler
    │   ├── createuserhandler.go
    │   ├── getuserhandler.go
    │   ├── qiuniutokenhandler.go
    │   └── routes.go
    ├── logic
    │   ├── createuserlogic.go
    │   ├── getuserlogic.go
    │   └── qiuniutokenlogic.go
    ├── svc
    │   └── servicecontext.go
    └── types
        └── types.go
````
# 4、由于 `user` 模块属于 `datacenter` 服务,因此先编写 `user.api` 文件
* 生成 `user.api` 文件
````shell
goctl api -o user.api
````
* 编写 `user.api` 文件
````shell
//注册请求
type RegisterReq struct {
	// TODO: add members here and delete this comment
	Mobile   string `json:"mobile"` //基本一个手机号码就完事
	Password string `json:"password"`
	Smscode	string `json:"smscode"` //短信码
}
//登陆请求
type LoginReq struct{
	Mobile   string `json:"mobile"`
	Type int64 `json:"type"`	//1.密码登陆，2.短信登陆
	Password string `json:"password"`
}
//微信登陆
type WxLoginReq struct {
	Beid	  int64  `json:"beid"` //应用id
	Code string `json:"code"` //微信登陆密钥
	Ptyid	  int64  `json:"ptyid"` //对应平台
}

//返回用户信息
type UserReply struct {
	Auid       int64  `json:"auid"`
	Uid       int64  `json:"uid"`
	Beid	  int64  `json:"beid"` //应用id
	Ptyid	  int64  `json:"ptyid"` //对应平台
	Username string `json:"username"`
	Mobile   string `json:"mobile"`
	Nickname string `json:"nickname"`
	Openid string `json:"openid"`
	Avator string `json:"avator"`
	JwtToken
}
//返回APPUser
type AppUser struct{
	Uid       int64  `json:"uid"`
	Auid       int64  `json:"auid"`
	Beid	  int64  `json:"beid"` //应用id
	Ptyid	  int64  `json:"ptyid"` //对应平台
	Nickname string `json:"nickname"`
	Openid string `json:"openid"`
	Avator string `json:"avator"`
}

type LoginAppUser struct{
	Uid       int64  `json:"uid"`
	Auid       int64  `json:"auid"`
	Beid	  int64  `json:"beid"` //应用id
	Ptyid	  int64  `json:"ptyid"` //对应平台
	Nickname string `json:"nickname"`
	Openid string `json:"openid"`
	Avator string `json:"avator"`
	JwtToken
}

type JwtToken struct {
	AccessToken  string `json:"access_token,omitempty"`
	AccessExpire int64  `json:"access_expire,omitempty"`
	RefreshAfter int64  `json:"refresh_after,omitempty"`
}

type UserReq struct{
	Auid       int64  `json:"auid"`
	Uid       int64  `json:"uid"`
	Beid	  int64  `json:"beid"` //应用id
	Ptyid	  int64  `json:"ptyid"` //对应平台
}

// 声明一个服务组，包名为 `user`
@server(
	group: user //包名为`user`
)
// 以下所有的代码都会在 `user`这个文件夹下自动生成
service datacenter-api {
	@handler ping
	post /user/ping ()
	
	@handler register
	post /user/register (RegisterReq) returns (UserReply)
	
	@handler login
	post /user/login (LoginReq) returns (UserReply)
	
	@handler wxlogin
	post /user/wx/login (WxLoginReq) returns (LoginAppUser)
	
	@handler code2Session
	get /user/wx/login () returns (LoginAppUser)
}
// 为 userInfo 添加一个服务
// userInfo 的jwt鉴权是 Auth
// userInfo 属于 user 这个包
// userInfo 的中间件是 Usercheck
@server(
	jwt: Auth
	group: user
	middleware: Usercheck //
)
service datacenter-api {
	@handler userInfo
	get /user/dc/info (UserReq) returns (UserReply)
}
````
# 5、将 `user` 模块导入 `datacenter` 服务中

````shell
syntax = "v1"

info(
	title: "中台系统"// TODO: add title
	desc: "中台系统"// TODO: add description
	author: "li ou"
	email: "3212728662@qq.com"
)

// 导入 user 模块
import "user.api"


// 获取应用信息
type Beid {
	Beid int64 `json:"beid"`
}
// 返回值
type Token {
	Token string `json:"token"`
}
// 微信活动
type WxTicket {
	Ticket string `json:"ticket"`
}

// 应用信息
type Application {
	Sname       string `json:"Sname"`       //名称
	Logo        string `json:"logo"`        //login
	IsClose     int64  `json:"isclose"`     //是否关闭
	Fullwebsite string `json:"fullwebsite"` //全站名称
}

//获取分享的请求参数
type SnsReq {
	Beid
	Ptyid   int64  `json:"ptyid"`    //对应平台
	BackUrl string `json:"back_url"` //登陆返回的地址
}

type SnsResp {
	Beid
	Ptyid    int64  `json:"ptyid"`     //对应平台
	Appid    string `json:"appid"`     //sns 平台的id
	Title    string `json:"title"`     //名称
	LoginUrl string `json:"login_url"` //微信登陆的地址
}

//获取分享的返回数据
type WxShareResp {
	Appid     string `json:"appid"`
	Timestamp int64  `json:"timestamp"`
	Noncestr  string `json:"noncestr"`
	Signature string `json:"signature"`
}

// 这是 datacenter 这个服务最外部的http服务
service datacenter-api {
	@doc(
		summary: "七牛云上传凭证"
	)
	@handler qiuniuToken
	post /common/qiuniu/token (Beid) returns (Token)
}
````

# 6、生成 `user` 模块 
* 在 datacenter 目录下执行命令
````shell
goctl api go -api datacenter.api -dir .
````
* 生成的代码目录结构如下
````shell
.
├── READEME.md
├── datacenter.api
├── datacenter.go
├── etc
│   └── datacenter-api.yaml
├── go.mod
├── go.sum
├── internal
│   ├── config
│   │   └── config.go
│   ├── handler
│   │   ├── createuserhandler.go
│   │   ├── getuserhandler.go
│   │   ├── qiuniutokenhandler.go
│   │   ├── routes.go
│   │   └── user
│   │       ├── code2sessionhandler.go
│   │       ├── loginhandler.go
│   │       ├── pinghandler.go
│   │       ├── registerhandler.go
│   │       ├── userinfohandler.go
│   │       └── wxloginhandler.go
│   ├── logic
│   │   ├── createuserlogic.go
│   │   ├── getuserlogic.go
│   │   ├── qiuniutokenlogic.go
│   │   └── user
│   │       ├── code2sessionlogic.go
│   │       ├── loginlogic.go
│   │       ├── pinglogic.go
│   │       ├── registerlogic.go
│   │       ├── userinfologic.go
│   │       └── wxloginlogic.go
│   ├── middleware
│   │   └── usercheckmiddleware.go
│   ├── svc
│   │   └── servicecontext.go
│   └── types
│       └── types.go
└── user.api

````

# 7 、新增 `common` 包 (`common` 包属于 `datacenter` 内部的公共服务，因此在 `datacenter.api` 文件中新增代码即可)
* 在 `datacenter.api` 中新增代码
````shell
syntax = "v1"

info(
	title: "中台系统"// TODO: add title
	desc: "中台系统"// TODO: add description
	author: "li ou"
	email: "3212728662@qq.com"
)

// 导入 user 模块
import "user.api"

// 获取应用信息
type Beid {
	Beid int64 `json:"beid"`
}
// 返回值
type Token {
	Token string `json:"token"`
}
// 微信活动
type WxTicket {
	Ticket string `json:"ticket"`
}

// 应用信息
type Application {
	Sname       string `json:"Sname"`       //名称
	Logo        string `json:"logo"`        //login
	IsClose     int64  `json:"isclose"`     //是否关闭
	Fullwebsite string `json:"fullwebsite"` //全站名称
}

//获取分享的请求参数
type SnsReq {
	Beid
	Ptyid   int64  `json:"ptyid"`    //对应平台
	BackUrl string `json:"back_url"` //登陆返回的地址
}

type SnsResp {
	Beid
	Ptyid    int64  `json:"ptyid"`     //对应平台
	Appid    string `json:"appid"`     //sns 平台的id
	Title    string `json:"title"`     //名称
	LoginUrl string `json:"login_url"` //微信登陆的地址
}

//获取分享的返回数据
type WxShareResp {
	Appid     string `json:"appid"`
	Timestamp int64  `json:"timestamp"`
	Noncestr  string `json:"noncestr"`
	Signature string `json:"signature"`
}

//新增 common 包
//datacenter/internal 下会多出一个 common api服务
@server(
	group: common
)

//以下所有api接口都会在 common 包下自动生成
service datacenter-api {
	@doc(
		summary: "获取站点的信息"
	)
	@handler appInfo
	get /common/appinfo (Beid) returns (Application)
	@doc(
		summary: "获取站点的社交属性信息"
	)
	@handler snsInfo
	post /common/snsinfo (SnsReq) returns (SnsResp)

	//获取分享的
	@handler wxTicket
	post /common/wx/ticket (SnsReq) returns (WxShareResp)
}

//上传需要登陆
@server(
	jwt: Auth
	group: common
)
// 这是 datacenter 这个服务最外部的http服务
service datacenter-api {
	@doc(
		summary: "七牛云上传凭证"
	)
	@handler qiuniuToken
	post /common/qiuniu/token (Beid) returns (Token)
}
````
* 在终端执行命令
````shell
goctl api go -api datacenter.api -dir .
````
* 生成的代码目录结构如下
````shell
.
├── READEME.md
├── datacenter.api
├── datacenter.go
├── etc
│   └── datacenter-api.yaml
├── go.mod
├── go.sum
├── internal
│   ├── config
│   │   └── config.go
│   ├── handler
│   │   ├── common
│   │   │   ├── appinfohandler.go
│   │   │   ├── qiuniutokenhandler.go
│   │   │   ├── snsinfohandler.go
│   │   │   └── wxtickethandler.go
│   │   ├── createuserhandler.go
│   │   ├── getuserhandler.go
│   │   ├── qiuniutokenhandler.go
│   │   ├── routes.go
│   │   └── user
│   │       ├── code2sessionhandler.go
│   │       ├── loginhandler.go
│   │       ├── pinghandler.go
│   │       ├── registerhandler.go
│   │       ├── userinfohandler.go
│   │       └── wxloginhandler.go
│   ├── logic
│   │   ├── common
│   │   │   ├── appinfologic.go
│   │   │   ├── qiuniutokenlogic.go
│   │   │   ├── snsinfologic.go
│   │   │   └── wxticketlogic.go
│   │   ├── createuserlogic.go
│   │   ├── getuserlogic.go
│   │   ├── qiuniutokenlogic.go
│   │   └── user
│   │       ├── code2sessionlogic.go
│   │       ├── loginlogic.go
│   │       ├── pinglogic.go
│   │       ├── registerlogic.go
│   │       ├── userinfologic.go
│   │       └── wxloginlogic.go
│   ├── middleware
│   │   └── usercheckmiddleware.go
│   ├── svc
│   │   └── servicecontext.go
│   └── types
│       └── types.go
└── user.api
````
# 8、填充 `datacenter/internal/common` 的业务逻辑
````go

````
# 9、编译 `goctl-swagger`插件 
* [goctl-swagger插件](https://github.com/zeromicro/goctl-swagger)
````shell
go get -u github.com/zeromicro/goctl-swagger
````
* 使用方式
````go
info(
	title: "type title here"
	desc: "type desc here"
	author: "type author here"
	email: "type email here"
	version: "type version here"
)


type (
	RegisterReq {
		Username string `json:"username"`
		Password string `json:"password"`
		Mobile string `json:"mobile"`
	}
	
	LoginReq {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	
	UserInfoReq {
		Id string `path:"id"`
	}
	
	UserInfoReply {
		Name string `json:"name"`
		Age int `json:"age"`
		Birthday string `json:"birthday"`
		Description string `json:"description"`
		Tag []string `json:"tag"`
	}
	
	UserSearchReq {
		KeyWord string `form:"keyWord"`
	}
)

service user-api {
	@doc(
		summary: "注册"
	)
	@handler register
	post /api/user/register (RegisterReq)
	
	@doc(
		summary: "登录"
	)
	@handler login
	post /api/user/login (LoginReq)
	
	@doc(
		summary: "获取用户信息"
	)
	@handler getUserInfo
	get /api/user/:id (UserInfoReq) returns (UserInfoReply)
	
	@doc(
		summary: "用户搜索"
	)
	@handler searchUser
	get /api/user/search (UserSearchReq) returns (UserInfoReply)
}
````
* 生成swagger.json 文件
````shell
goctl api plugin -plugin goctl-swagger="swagger -filename user.json" -api user.api -dir .
````
* 目录结构如下:
````shell
.
├── LICENSE
├── READEME.md
├── datacenter.api
├── datacenter.go
├── etc
│   └── datacenter-api.yaml
├── go.mod
├── go.sum
├── internal
│   ├── config
│   │   └── config.go
│   ├── handler
│   │   ├── common
│   │   │   ├── appinfohandler.go
│   │   │   ├── qiuniutokenhandler.go
│   │   │   ├── snsinfohandler.go
│   │   │   └── wxtickethandler.go
│   │   ├── routes.go
│   │   └── user
│   │       ├── code2sessionhandler.go
│   │       ├── loginhandler.go
│   │       ├── pinghandler.go
│   │       ├── registerhandler.go
│   │       ├── userinfohandler.go
│   │       └── wxloginhandler.go
│   ├── logic
│   │   ├── common
│   │   │   ├── appinfologic.go
│   │   │   ├── qiuniutokenlogic.go
│   │   │   ├── snsinfologic.go
│   │   │   └── wxticketlogic.go
│   │   └── user
│   │       ├── code2sessionlogic.go
│   │       ├── loginlogic.go
│   │       ├── pinglogic.go
│   │       ├── registerlogic.go
│   │       ├── userinfologic.go
│   │       └── wxloginlogic.go
│   ├── middleware
│   │   └── usercheckmiddleware.go
│   ├── svc
│   │   └── servicecontext.go
│   └── types
│       └── types.go
├── sql.sql
├── user.api
└── user.json
````
* 指定Host
````shell
$ goctl api plugin -plugin goctl-swagger="swagger -filename user.json -host 127.0.0.2 -basepath /api" -api user.api -dir .
````
* swagger ui 查看生成的文档
````shell
 $ docker run --rm -p 8083:8080 -e SWAGGER_JSON=/foo/user.json -v $PWD:/foo swaggerapi/swagger-ui
````
